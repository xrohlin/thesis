Final thesis
===============================

[Built version](https://gitlab.fi.muni.cz/api/v4/projects/37593/jobs/artifacts/main/raw/thesis.pdf?job=typeset)

Style based on [zapr](https://gitlab.fi.muni.cz/xmatous3/zapr) template by Adam Matoušek.
